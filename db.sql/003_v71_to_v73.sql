create view square.v_dataquier as
with varattrs(fk_variable, codes) as (
  select fk_variable, string_agg(name || '=' || value, '|')
  from square.t_variableattribute
  group by fk_variable
), missings(fk_element, codes, is_jump) as (
  select ml.fk_element, string_agg(m.code || '=' || m.name_in_study, '|'), t."name" = 'jump'
  from square.t_missinglist ml
  left join square.t_missing m on m.fk_missinglist = ml.pk
  left join square.t_missingtype t on t.pk = m.fk_missingtype 
  group by ml.fk_element, t.name
)
select e.unique_name as VAR_NAMES,
       t1.value as LABEL,
       t2.value as LABEL_DE,
       v.datatype as DATA_TYPE,
       v.valuelist as VALUE_LABELS,
       m.codes as MISSING_LIST,
       j.codes as JUMP_LIST,
       pe1.unique_name as KEY_OBSERVER,
       pe2.unique_name as KEY_DEVICE,
       pe3.unique_name as KEY_DATETIME,
       p.unique_name as KEY_STUDY_SEGMENT,
       e.order_nr as VARIABLE_ORDER,
       a.codes as VAR_ATTRS
from square.t_variable v
left join varattrs a on a.fk_variable = v.fk_element 
left join square.t_element e on e.pk = v.fk_element 
left join square.t_element p on p.pk = e.fk_parent 
left join square.t_translation t1 on t1.fk_lang = e.fk_lang and t1.isocode = 'en'
left join square.t_translation t2 on t2.fk_lang = e.fk_lang and t2.isocode = 'de_DE'
left join square.t_metadatatype pt1 on pt1."name" = 'observer'
left join square.t_confounder pv1 on pv1.fk_element = e.pk and pv1.fk_metadatatype = pt1.pk 
left join square.t_element pe1 on pe1.pk = pv1.fk_referenced_element 
left join square.t_metadatatype pt2 on pt2."name" = 'observation starttime'
left join square.t_confounder pv2 on pv2.fk_element = e.pk and pv2.fk_metadatatype = pt2.pk 
left join square.t_element pe2 on pe2.pk = pv2.fk_referenced_element 
left join square.t_metadatatype pt3 on pt3."name" = 'device'
left join square.t_confounder pv3 on pv3.fk_element = e.pk and pv3.fk_metadatatype = pt3.pk 
left join square.t_element pe3 on pe3.pk = pv3.fk_referenced_element 
left join missings m on m.fk_element = v.fk_element and m.is_jump is false
left join missings j on j.fk_element = v.fk_element and j.is_jump is true
;
drop view if exists square.r_matrixparentparam;
create or replace view square.r_matrixparentparam as 
select fi.input_name, 
       case when pmcp.value is null
       then null 
       else jsonb_build_object('value', pmcp.value, 'refers_metadata', pmcp.refers_metadata, 'type_name', fit.name)
       end as value, 
       fi.ordering,
       mc.name as alias,
       r.pk as fk_report,
       fi.type_restriction
from square.t_matrixcolumnparameter pmcp
left join square.t_matrixcolumn mc on mc.pk = pmcp.fk_matrixcolumn
left join square.t_report p on p.fk_functionlist = mc.fk_functionlist and p.pk is null
left join square.t_report r on r.fk_functionlist = mc.fk_functionlist and r.pk is not null
left join square.t_functioninput fi on fi.pk = pmcp.fk_functioninput
left join square.t_functioninputtype fit on fit.pk = fi.fk_type
where pmcp.fk_report is null;

grant select on square.r_matrixparentparam to interface_rserver;
