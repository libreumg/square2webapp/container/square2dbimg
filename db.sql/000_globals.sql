CREATE ROLE henkej;
CREATE ROLE interface_pgbouncer;
CREATE ROLE interface_rest;
CREATE ROLE interface_rserver;
ALTER ROLE interface_rserver WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:cvkvQ2wx0PV+SSvLkCb
kOA==$wN6k/vUKS6HhM0VJE9aAC98kvX2TAaBwUaLWgRqTx/k=:NFmiDJ87MRQjXBcGeuLFc8jvNKeFx3Rie3hHyCRUOCI=';
CREATE ROLE interface_square;
ALTER ROLE interface_square WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:F83UinMtYu7ndj8Kgn2C
iQ==$lx1WUPWyuHvmf3CsmMT3g90O2aQpHR8mYgnhRsRQRJM=:6HZsIr2/njvEBE1K2N1TZR6nK2mZe4TmLICrnYD0EiY=';
CREATE ROLE interface_viewbuilder;
ALTER ROLE interface_viewbuilder WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:F+BCEeOywZwAfTh
vJKq6Yw==$yv1wM/3+CM6Ve9zVQ55hmfmv3SmUu6VfSFAWSMgNGEU=:/RmczdMemmCnHXTUBolH2W/FRqgM1z6PZjVB4kK+oDI=';
CREATE ROLE jooq;

ALTER ROLE interface_rserver SET search_path TO 'square', 'squareout';
ALTER ROLE interface_square SET search_path TO 'square';

alter role interface_rserver encrypted password 'initial';
alter role interface_square encrypted password 'initial';
