# build the image

You can build the image by simply cloning this repository to a folder on your local disk. Then, run

```bash
docker build --tag=squaredb .
```

to build the image with the tag `squaredb` (you are free to choose another name of course).

# run the image

After building the image locally, one can start it with

```bash
docker run -e POSTGRES_PASSWORD=password squaredb
```
. You can use another password as you like.

# connect to the database

To get access to the database directly, you can connect to the docker instance, become the user `postgres` and run `psql`:

```bash
docker exec -it <HOWEVERYOURDOCKERNAMESIT> /bin/bash
root@docker:/ su - postgres
postgres@docker:/ psql
```

Then, you can use all `psql` commands as usually.
